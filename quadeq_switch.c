#include <stdio.h>
#include <math.h>

int main()
{
    int a,b,c,d,e;
    float r1,r2;
    printf("enter the coefficients of quadractic eq a,b,c");
    scanf("%d%d%d",&a,&b,&c);
    d=(((b*b)-4*a*c));
    if(d<0)
        e=1;
    else if (d>0)
        e=2;
    else
        e=3;
    r1=((-b)+sqrt(d))/(2*a);
    r2=((-b)-sqrt(d))/(2*a);
    switch(e)
    {
    case 1:printf("The roots are imaginary");
           break;
    case 2:printf("The roots are real and roots are %f,%f",r1,r2);
           break;
    case 3:printf("The roots are equal and the root is %f",r1);
           break;
    }
    return 0;
}
