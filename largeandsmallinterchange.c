#include<stdio.h>
int main()
{
    int n,large=0,small,small_pos,large_pos,i;
    printf("enter the no. of elements in an array\n");
    scanf("%d",&n);
    int a[n];
    printf("enter the elements\n");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    for(i=0;i<n;i++)
    {
        if(a[i]>large)
        {
            large=a[i];
            large_pos=i+1;
        }
    }
    small=a[0];
    for(i=0;i<n;i++)
    {
        if(a[i]<small)
        {
            small=a[i];
            small_pos=i+1;
        }
    }
    printf("ARRAY BEFORE INTERCHANGE:");
    for(i=0;i<n;i++)
        printf("%d,",a[i]);
    printf("\n");
    printf("%d IS THE LARGEST NUMBER AND ITS POSITION IS %d\n",large,large_pos);
    printf("%d IS THE SMALLEST NUMBER AND ITS POSITION IS %d\n",small,small_pos);
    a[large_pos-1]=a[large_pos-1]+a[small_pos-1];
    a[small_pos-1]=a[large_pos-1]-a[small_pos-1];
    a[large_pos-1]=a[large_pos-1]-a[small_pos-1];
    printf("ARRAY AFTER INTERCHANGING SMALLEST AND LARGEST:");
    for(i=0;i<n;i++)
        printf("%d, ",a[i]);
    return 0;
}