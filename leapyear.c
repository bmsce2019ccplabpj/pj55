#include <stdio.h>
void input(int *a)
{
    printf("enter the year");
    scanf("%d",a);
}    
void compute(int *a,int *b)
{
    if(*a%4==0)
    *b=1;
    else
    *b=0;
}    
int main()
{
    int yr,a;
    input(&yr);
    compute(&yr,&a);
    if(a==1)
    printf("%d is a leap year",yr);
    else
    printf("%d is not a leap year",yr);
    return 0;
}    